export abstract class WdioHelper {

    get timeoutDefault() { return 10000; }
    get timeoutImport() { return 120000; }
    get timeoutSubmitOrder() { return 60000; }

    get $loadSpinner() { return $('.loader-backdrop'); }

    //
    // Browser Actions
    //

    protected async setUrlWaitUntilDisplayed(url: string, displayElement: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        await browser.url(url);
        await this.waitUntilDisplayed(displayElement);
    }

    protected async setUrlWaitUntilAnyDisplay(url: string, elements: WebdriverIO.Element[], timeout: number = this.timeoutDefault) {
        await browser.url(url);
        await this.waitUntilAnyDisplay(elements);
    }

    protected async getUrl() {
        await browser.getUrl();
    }

    protected async closeBrowser() {
        await browser.closeWindow();
    }

    protected async switchToWindow(newUrlorTitle: any) {
        await browser.switchWindow(newUrlorTitle)
    }

    protected async setWindowSize(width: number, height: number) {
        await browser.setWindowSize(width, height);
    }

    protected async pageRefresh() {
        await browser.reloadSession();
    }

    protected async switchToFrame(id: number | object | null) {
        await browser.switchToFrame(id);
    }

    //
    // Element Actions
    //

    protected async clickAndWaitUntilDisplayed(clickElement: WebdriverIO.Element, displayElement: WebdriverIO.Element, timeout: number = this.timeoutDefault,) {
        //await this.switchToFrame(iframe);
        await this.click(clickElement);
        await this.waitUntilDisplayed(displayElement, timeout);
        //await this.switchToFrame(null);
    }

    protected async clickAndWaitUntilNotDisplayed(clickElement: WebdriverIO.Element, displayElement: WebdriverIO.Element, timeout: number = this.timeoutDefault,) {
        //await this.switchToFrame(iframe);
        await this.click(clickElement);
        await this.waitUntilNotDisplayed(displayElement, timeout);
        //await this.switchToFrame(null);
    }

    protected async clickAndWaitUntilAnyDisplay(clickElement: WebdriverIO.Element, displayElements: WebdriverIO.Element[], timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.click(clickElement);
        await this.waitUntilAnyDisplay(displayElements, timeout);
        //await this.switchToFrame(null);
    }

    protected async clickAndWaitUntilClickable(clickElement: WebdriverIO.Element, clickableElement: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.click(clickElement);
        await this.waitUntilClickable(clickableElement, timeout);
        //await this.switchToFrame(null);
    }

    protected async clickAndWaitUntilNotClickable(clickElement: WebdriverIO.Element, clickableElement: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.click(clickElement);
        await this.waitUntilNotClickable(clickableElement, timeout);
        //await this.switchToFrame(null);
    }

    protected async clickAndOnlyClick(clickElement: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.waitUntilNotClickable(clickElement, timeout);
        //await this.switchToFrame(null);
    }

    protected async doubleClick(clickElement: WebdriverIO.Element) {
        //await this.switchToFrame(iframe);
        await this.click(clickElement);
        await this.click(clickElement);
        //await this.switchToFrame(null);
    }

    protected async setValue(element: WebdriverIO.Element, value: string) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.setValue(value);
       // await this.switchToFrame(null);
    }

    protected async addValue(element: WebdriverIO.Element, value: string) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.addValue(value);
        //await this.switchToFrame(null);
    }

    protected async selectByValue(element: WebdriverIO.Element, value: string) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.selectByAttribute('value', value);
        //await this.switchToFrame(null);
    }

    protected async selectByIndex(element: WebdriverIO.Element, value: number) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.selectByIndex(value);
        //await this.switchToFrame(null);
    }

    protected async waitUntilDisplayed(element: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.waitForDisplayed({ timeout, });
        //await this.switchToFrame(null);
    }

    protected async waitUntilNotDisplayed(element: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.waitForDisplayed({ timeout, reverse: true });
        //await this.switchToFrame(null);
    }

    protected async waitUntilAnyDisplay(elements: WebdriverIO.Element[], timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await browser.waitUntil(async () => {
            let elementFound = false;

            while (elementFound === false) {
                for (const element of elements) {
                    elementFound = await this.isDisplayed(element);
                    if(elementFound){
                        break;
                    }
                }
            }
            return true;
        }, { timeout });
        //await this.switchToFrame(null);
    }

    protected async waitUntilEnabled(element: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.waitForEnabled({ timeout });
        //await this.switchToFrame(null);
    }

    protected async isDisplayed(element: WebdriverIO.Element) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        //await this.switchToFrame(null);
        return element.isDisplayed();
        
    }

    protected async isSelected(element: WebdriverIO.Element) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        //await this.switchToFrame(null);
        return element.isSelected();
    }

    protected async waitUntilClickable(element: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.waitForClickable({ timeout });
        //await this.switchToFrame(null);
    }

    protected async waitUntilNotClickable(element: WebdriverIO.Element, timeout: number = this.timeoutDefault) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        await element.waitForClickable({ timeout, reverse: true });
        //await this.switchToFrame(null);
    }

    protected async getAttribute(element: WebdriverIO.Element, attribute: string) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        //await this.switchToFrame(null);
        return element.getAttribute(attribute);
        
    }

    protected async getText(element: WebdriverIO.Element) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        //await this.switchToFrame(null);
        return element.getText();
        
    }

    protected async getValue(element: WebdriverIO.Element) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        //await this.switchToFrame(null);
        return element.getValue();
    }

    protected async getImgSize(element: WebdriverIO.Element) {
        //await this.switchToFrame(iframe);
        await this.checkAndWaitLoadSpinner();
        //await this.switchToFrame(null);
        return element.getSize();
    }




    //
    // Private
    //

    private async checkAndWaitLoadSpinner() {
        // if ((this.$loadSpinner).isDisplayed()) {
        //     await (this.$loadSpinner).waitForDisplayed({ reverse: true });
        // }
    }

    private async click(element: WebdriverIO.Element) {
        await this.checkAndWaitLoadSpinner();
        await element.click();
    }
}
