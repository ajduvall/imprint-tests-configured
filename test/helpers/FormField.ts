import { FieldType } from './FieldType';

export abstract class FormField {
    formSection: string;
    type: string;
    optionValue?: string;

    constructor(type: string = FieldType.Input, optionValue?: string) {
        this.type = type;
        this.optionValue = optionValue;
    }
}
