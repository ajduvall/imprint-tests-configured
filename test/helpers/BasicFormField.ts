import { FormField } from './FormField';
import { FieldType } from './FieldType';

export class BasicFormField extends FormField {

    name: string;

    constructor(name: string, type: string = FieldType.Input, optionValue?: string) {
        super(type, optionValue);
        this.name = name;
    }
}