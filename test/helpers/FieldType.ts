export enum FieldType {
    Checkbox = 'checkbox',
    Input = 'input',
    Radio = 'radio',
    Select = 'select'
}