import { WdioHelper } from './wdioHelper';
import { FormField } from './FormField';
import { BasicFormField } from './BasicFormField';
import { FieldType } from './FieldType';
import { Element } from 'WebDriverIO';

export abstract class FormHelper extends WdioHelper {

    protected async setElementValue(field: FormField, element: WebdriverIO.Element, value: string) {
        await this.waitUntilEnabled(element);

        if (field.type === FieldType.Input) {
            await this.setValue(element, value);
        } 
        else if (field.type === FieldType.Checkbox || field.type === FieldType.Radio) {
            // While loop for 941 some checkboxes don't select on first click
            while ((await this.isSelected(element)).toString() !== value) {
                await this.clickAndWaitUntilDisplayed(element, element);
            }
        } 
        else if (field.type === FieldType.Select) {
            await this.selectByValue(element, value);
        }
    }
}    

//changed protected async setElementValue(field: FormField, element: Element, value: string) to protected async setElementValue(field: FormField, element: WebdriverIO.Element, value: string)