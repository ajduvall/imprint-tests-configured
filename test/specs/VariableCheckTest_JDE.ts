import { HomePage } from "../pageobjects/HomePage";
import { ImprintApp } from "../pageobjects/ImprintApp/ImprintApp";
import { ProductPage } from "../pageobjects/ProductPage";
import { ShoppingCart } from "../pageobjects/ShoppingCart";
import { Credentials_Protected } from "../SupportLibraries/Credentials_Protected";
import { JDEHomePage } from "../pageobjects/JDEHomePage";
import { CustomerInfoJDE } from "../pageobjects/JDE/CustomerInfoJDE";
import { LoginJDE } from "../pageobjects/JDE/LoginJDE";
import { SalesOrderJDE } from "../pageobjects/JDE/SalesOrderJDE";
import { NavJDE } from "../pageobjects/JDE/NavJDE";

const imprintApp = new ImprintApp();
const credentials = new Credentials_Protected();
const jdeHomePage = new JDEHomePage();
const customerInfoJDE = new CustomerInfoJDE();
const loginJDE = new LoginJDE();
const salesOrderJDE = new SalesOrderJDE();
const navJde = new NavJDE();

describe ('Imprint should', async () => {

    const using = require(`../SupportLibraries/using.js`);
    
    beforeAll(async () => {
        const logInCredentials: any = await credentials.getLogin();
        await loginJDE.navigateTo();
        await loginJDE.logIn(logInCredentials.userName, logInCredentials.password);
        await customerInfoJDE.selectSalesOrderTemplate();
        await customerInfoJDE.navigateTo();
        await customerInfoJDE.addCustomerInfo("1294124", "2");
    })

    using(require(`../datadrivers/MassOutputTest_JDE/MassCheckOutput_BottomChecks.json`), (checkData: any) => {
        
   
            for(const product of checkData.products) {
        
                    it(`${checkData.description} - ${product.productID} - ${product.expectedResultForAddressLine}`, async () => {
                        await salesOrderJDE.inputProductInfo(`${product.productID}`, "500");
                        
                        const browserHandles = await browser.getWindowHandles();
                        console.log(browserHandles);
                        await browser.switchToWindow(await browserHandles[1])
                        
                        await salesOrderJDE.openImprintFromWebImprintHistory();

                        let browserHandlesTwo = await browser.getWindowHandles();
                        console.log(browserHandlesTwo);
                        await browser.switchToWindow(await browserHandlesTwo[2]);

                        //Company Information
                        await imprintApp.companyInformation.open();
                        await imprintApp.companyInformation.ifDisplayedSetAddressBlockValue(`${product.expectedResultForAddressLine}`);
                        
                        //Bank Information
                        await imprintApp.bankInformation.isDisplayedOpen();
                        await imprintApp.bankInformation.isDisplayedInputValidBankInformation();

                        //Voucher Details
                        await imprintApp.voucherDetails.open();
                        await imprintApp.voucherDetails.ifDisplayedSetVoucherLineOne("Voucher Line One Test");
                        await imprintApp.voucherDetails.ifDisplayedSetVoucherLineTwo("Voucher Line Two Test");

                        //Signature Area
                        await imprintApp.signatureArea.open();
                        await imprintApp.signatureArea.ifDisplayedSetNumberofSignatureLines(2);
                        await imprintApp.signatureArea.ifDisplayedSetSignatureLineOneLabel("Signature Line One Label");
                        await imprintApp.signatureArea.ifDisplayedSetSignatureLineTwoLabel("Signature Line Two Label");
                        await imprintApp.signatureArea.ifDisplayedSetSignatureHeadingOneLabel("Signature Heading One");
                        await imprintApp.signatureArea.ifDisplayedSetSignatureHeadingTwoLabel("Signature Heading Two");
                
                        //Approval
                        await imprintApp.approval.open();
                        await imprintApp.approval.addInitials("SEL");
                        await imprintApp.approval.approveOrderTwo();
                        //Close, Save, and Print Preview Buttons
                
                        //Help

                        //Navigate Back to JDE Order
                        await browser.closeWindow();
                        await browser.switchToWindow(await browserHandlesTwo[1]);
                        await browser.closeWindow();
                        await browser.switchToWindow(await browserHandlesTwo[0]);
                        await jdeHomePage.navigateBackToOrder();
                    })
                }
            })
})