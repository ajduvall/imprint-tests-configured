import { HomePage } from "../pageobjects/HomePage";
import { ImprintApp } from "../pageobjects/ImprintApp/ImprintApp";
import { ProductPage } from "../pageobjects/ProductPage";
import { ShoppingCart } from "../pageobjects/ShoppingCart";
import { Credentials } from "../SupportLibraries/Credentials";

const homePage = new HomePage();
const productPage = new ProductPage();
const shoppingCart = new ShoppingCart();
const imprintApp = new ImprintApp();
const credentials = new Credentials();

describe ('Imprint should', async () => {

    beforeAll(async () => {
        const logInCredentials: any = await credentials.getLogin();
        await homePage.navigateTo();
        await homePage.logIn(logInCredentials.userName, logInCredentials.password); 
    })
    
    beforeEach(async () => {
        await homePage.navigateTo();
    })

    afterEach(async () => {
        await homePage.navigateTo();
        await productPage.moveToCart();
        await shoppingCart.clearCart();
    })

    it('output a check: address block: stock logo: starting check no: "500" productID: L1302', async () => {
        await homePage.setSearchValue("L1302");
        await productPage.setCheckDropDowns();
        await productPage.setQuantity("500");
        await productPage.addToCart();
        await productPage.moveToCart();
        
        const returnPageTitle = (await shoppingCart.getPageTitle()).toString();
        
        await shoppingCart.openImprintFromCart();
        await imprintApp.productSelection.setQuantity("1000");
        await imprintApp.companyInformation.open()
        await imprintApp.companyInformation.checkCustomerServiceRequestBox();
        await imprintApp.companyInformation.setAddressBlockValue("L1302 WebDriverIO Smoke Test");
        await imprintApp.companyInformation.selectStockLogo();
        await imprintApp.bankInformation.open();
        await imprintApp.bankInformation.inputValidBankInformation();
        await imprintApp.bankInformation.setStartingCheckNumber("500")
        await imprintApp.approval.open();
        await imprintApp.approval.addInitials("SEL");
        await imprintApp.approval.approveOrder();
        await shoppingCart.switchToShoppingCart(returnPageTitle);
    })

    it('output a noncheck: folder productID: FLDR410', async () => {
        await homePage.setSearchValue("FLDR410");
        await productPage.setNonCheckDropDowns();
        await productPage.setQuantity("50");
        await productPage.addToCart();
        await productPage.moveToCart();

        const returnPageTitle = (await shoppingCart.getPageTitle()).toString();

        await shoppingCart.openImprintFromCart();
        await imprintApp.companyInformation.open()
        await imprintApp.companyInformation.setAddressBlockValue("FLDR410 Selenium Test")
        await imprintApp.approval.open();
        await imprintApp.approval.addInitials("SEL");
        await imprintApp.approval.approveOrder();
        await shoppingCart.switchToShoppingCart(returnPageTitle);
    })
})
