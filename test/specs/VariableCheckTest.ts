import { HomePage } from "../pageobjects/HomePage";
import { ImprintApp } from "../pageobjects/ImprintApp/ImprintApp";
import { ProductPage } from "../pageobjects/ProductPage";
import { ShoppingCart } from "../pageobjects/ShoppingCart";
import { Credentials } from "../SupportLibraries/Credentials";
//import * as using from '../SupportLibraries/using.js';
import { FileDirectoryHelper } from "../SupportLibraries/FileDirectoryHelper";

const homePage = new HomePage();
const productPage = new ProductPage();
const shoppingCart = new ShoppingCart();
const imprintApp = new ImprintApp();
const credentials = new Credentials();
//const productIDList = ["L1302", "L81064HB", "L1590", "L1089", "L1057", "L1512", "L1287", "L1090", "L1266", "L1750", "L1057", "5114", "1092", "L1512F", "L1237P"]
const fieldDirectoryHelper: FileDirectoryHelper = new FileDirectoryHelper();
//const dataDriver = require(`../datadrivers/MassCheckOutputTest.json`)

//module.exports = async (formName: string) => {

    describe("Output test of check:", async () => {
    
        const using = require(`../SupportLibraries/using.js`);

        beforeAll(async () => {
            const logInCredentials: any = await credentials.getLogin();
            await homePage.navigateTo();
            await homePage.logIn(logInCredentials.userName, logInCredentials.password); 
        })
        
        beforeEach(async () => {
            await homePage.navigateTo();
            await productPage.moveToCart();
            await shoppingCart.clearCartAll();
        })
    
        // afterEach(async () => {
        //     await homePage.navigateTo();
        //     await productPage.moveToCart();
        //     await shoppingCart.clearCart();
        // })
    
        //const dataDrivers = await fieldDirectoryHelper.getFilesInFolder(`../datadrivers/MassCheckOutputTest.json`);
    using(require(`../datadrivers/MassOutputTest_GreatlandNelco/MassCheckOutputTest_TopChecks.json`), (checkData: any) => {
        
        for(const product of checkData.products) {
    
                it(`${checkData.description} - ${product.productID} - ${product.expectedResultForAddressLine}`, async () => {
                    await homePage.setSearchValue(`${product.productID}`);
                    await productPage.setCheckDropDowns();
                    await productPage.setQuantity("500");
                    await productPage.addToCart();
                    await productPage.moveToCart();
                    
                    const returnPageTitle = (await shoppingCart.getPageTitle()).toString();
                    
                    await shoppingCart.openImprintFromCart();
                    
                    //Product Selection
                    //await imprintApp.productSelection.setQuantity("500");
                    //await imprintApp.productSelection.chooseSwatch();
                    //Choose Swatch?
                    
                    //Company Information
                    await imprintApp.companyInformation.open();
                    await imprintApp.companyInformation.ifDisplayedSetAddressBlockValue(`${product.expectedResultForAddressLine}`);
                    //await imprintApp.companyInformation.ifDisplayedSelectStockLogo();
                    //Custom Logo?
                    //Resize and Arrange Logos?
                    
                    //Bank Information
                    await imprintApp.bankInformation.isDisplayedOpen();
                    await imprintApp.bankInformation.isDisplayedInputValidBankInformation();
                    //await imprintApp.bankInformation.ifDisplayedSetStartingCheckNumber("500");
                    //Hide/Show MICR?
                    
                    //Voucher Details
                    //const voucherLineTwo = imprintApp.voucherDetails.$voucherLineTwo
                    await imprintApp.voucherDetails.open();
                    await imprintApp.voucherDetails.ifDisplayedSetVoucherLineOne("Voucher Line One Test");
                    await imprintApp.voucherDetails.ifDisplayedSetVoucherLineTwo("Voucher Line Two Test");

                    //Signature Area
                    await imprintApp.signatureArea.open();
                    await imprintApp.signatureArea.ifDisplayedSetNumberofSignatureLines(2);
                    await imprintApp.signatureArea.ifDisplayedSetSignatureLineOneLabel("Signature Line One Label");
                    await imprintApp.signatureArea.ifDisplayedSetSignatureLineTwoLabel("Signature Line Two Label");
                    await imprintApp.signatureArea.ifDisplayedSetSignatureHeadingOneLabel("Signature Heading One");
                    await imprintApp.signatureArea.ifDisplayedSetSignatureHeadingTwoLabel("Signature Heading Two");
            
                    //Approval
                    await imprintApp.approval.open();
                    await imprintApp.approval.addInitials("SEL");
                    await imprintApp.approval.approveOrderCloseWindow();
                    //Close, Save, and Print Preview Buttons
            
            
                    //Help
            
                    await shoppingCart.switchToShoppingCart(returnPageTitle);
                })
            // })
            }  
        })
        
    })
//}