import { HomePage } from "../pageobjects/HomePage";
import { ImprintApp } from "../pageobjects/ImprintApp/ImprintApp";
import { ProductPage } from "../pageobjects/ProductPage";
import { ShoppingCart } from "../pageobjects/ShoppingCart";
import { Credentials } from "../SupportLibraries/Credentials";

const homePage = new HomePage();
const productPage = new ProductPage();
const shoppingCart = new ShoppingCart();
const imprintApp = new ImprintApp();
const credentials = new Credentials();

describe('Bank Information tab should', async () => { 

    beforeAll(async () => {
        const logInCredentials: any = await credentials.getLogin();
        await homePage.navigateTo();
        await homePage.logIn(logInCredentials.userName, logInCredentials.password); 
    })
    
    beforeEach(async () => {
        await homePage.navigateTo();
    })

    afterEach(async () => {
        await homePage.navigateTo();
        await productPage.moveToCart();
        await shoppingCart.clearCart();
    })

    it('error with letters other than "o" and "x" in the Account Number field', async () => {
        await homePage.setSearchValue("L1302");
        await productPage.setCheckDropDowns();
        await productPage.setQuantity("500");
        await productPage.addToCart();
        await productPage.moveToCart();
        
        const returnPageTitle = (await shoppingCart.getPageTitle()).toString();
        
        await shoppingCart.openImprintFromCart();
        await imprintApp.bankInformation.open();
        await imprintApp.bankInformation.inputInvalidBankInformation("091000022", "abcd123");
        
        const alertdisplay = await imprintApp.bankInformation.isAlertDisplayed();
        expect(await alertdisplay).toBeTrue();               
    })

    it('error with no Bank Account Number', async () => {
        await homePage.setSearchValue("L1302");
        await productPage.setCheckDropDowns();
        await productPage.setQuantity("500");
        await productPage.addToCart();
        await productPage.moveToCart();
        
        const returnPageTitle = (await shoppingCart.getPageTitle()).toString();
        
        await shoppingCart.openImprintFromCart();
        await imprintApp.bankInformation.open();
        await imprintApp.bankInformation.inputInvalidBankInformation("091000022", "");
        await imprintApp.approval.open();
        await imprintApp.approval.addInitials("SEL");
        await imprintApp.approval.approveOrderExpectError();
        
        const alertdisplay = await imprintApp.bankInformation.isAlertDisplayed();
        expect(await alertdisplay).toBeTrue();
    })
    
    it('error with no Bank Routing Number', async () => {
        await homePage.setSearchValue("L1302");
        await productPage.setCheckDropDowns();
        await productPage.setQuantity("500");
        await productPage.addToCart();
        await productPage.moveToCart();
        
        const returnPageTitle = (await shoppingCart.getPageTitle()).toString();
        
        await shoppingCart.openImprintFromCart();
        await imprintApp.bankInformation.open();
        await imprintApp.bankInformation.inputInvalidBankInformation("", "x123456789o");
        await imprintApp.approval.open();
        await imprintApp.approval.addInitials("SEL");
        await imprintApp.approval.approveOrderExpectError();
        
        const alertdisplay = await imprintApp.bankInformation.isAlertDisplayed();
        expect(await alertdisplay).toBeTrue();
    })
})