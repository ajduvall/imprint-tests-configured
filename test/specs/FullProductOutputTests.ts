import { HomePage } from "../pageobjects/HomePage";
import { ImprintApp } from "../pageobjects/ImprintApp/ImprintApp";
import { ProductPage } from "../pageobjects/ProductPage";
import { ShoppingCart } from "../pageobjects/ShoppingCart";
import { Credentials } from "../SupportLibraries/Credentials";
//import { MassCheckOutputTest_2 } from "../datadrivers/MassCheckOutputTest_2"

const homePage = new HomePage();
const productPage = new ProductPage();
const shoppingCart = new ShoppingCart();
const imprintApp = new ImprintApp();
const credentials = new Credentials();
const productIDList = ["L1302"] //this test is hard coded to only suceed using L1302


describe("Output test of each", async () => {
    
    beforeAll(async () => {
        const logInCredentials: any = await credentials.getLogin();
        await homePage.navigateTo();
        await homePage.logIn(logInCredentials.userName, logInCredentials.password); 
    })
    
    beforeEach(async () => {
        await homePage.navigateTo();
    })

    afterEach(async () => {
        await homePage.navigateTo();
        await productPage.moveToCart();
        await shoppingCart.clearCart();
    })

    for(const product of productIDList) {

        it(`${product}`, async () => {
            await homePage.setSearchValue(`${product}`);
            await productPage.setCheckDropDowns();
            await productPage.setQuantity("250");
            await productPage.addToCart();
            await productPage.moveToCart();
            
            const returnPageTitle = (await shoppingCart.getPageTitle()).toString();
            
            await shoppingCart.openImprintFromCart();
            
            //Product Selection
            await imprintApp.productSelection.setQuantity("500");
            //await imprintApp.productSelection.chooseSwatch();
            //Choose Swatch?
            
            //Company Information
            await imprintApp.companyInformation.open()
            await imprintApp.companyInformation.setAddressBlockValue(`${product}`);
            await imprintApp.companyInformation.selectStockLogo();
            //Custom Logo?
            //Resize and Arrange Logos?
            
            //Bank Information
            await imprintApp.bankInformation.open();
            await imprintApp.bankInformation.inputValidBankInformation();
            await imprintApp.bankInformation.setStartingCheckNumber("500");
            //Hide/Show MICR?
            
            //Voucher Details
            await imprintApp.voucherDetails.open();
            await imprintApp.voucherDetails.setVoucherLineOne("Voucher Line One Test");
            await imprintApp.voucherDetails.setVoucherLineTwo("Voucher Line Two Test");
            await imprintApp.voucherDetails.setVoucherAlignmentCenter();
            
            //Signature Area
            await imprintApp.signatureArea.open();
            await imprintApp.signatureArea.setNumberofSignatureLines(2);
            await imprintApp.signatureArea.setSignatureLineOneLabel("Signature Line One Label");
            await imprintApp.signatureArea.setSignatureLineTwoLabel("Signature Line Two Label");
            await imprintApp.signatureArea.setSignatureHeadingOneLabel("Signature Heading One");
            await imprintApp.signatureArea.setSignatureHeadingTwoLabel("Signature Heading Two");
    
            //Approval
            await imprintApp.approval.open();
            await imprintApp.approval.addInitials("SEL");
            await imprintApp.approval.approveOrder();
            //Close, Save, and Print Preview Buttons
    
    
            //Help
    
            await shoppingCart.switchToShoppingCart(returnPageTitle);
        })
    }
})

