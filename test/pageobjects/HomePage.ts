import { WdioHelper } from '../helpers/wdioHelper';

export class HomePage extends WdioHelper {

    // Verify Docs page Loads
    private get $pageTitle() {return $("//head//title")}
    private get $homeTitle() {return $(`//span[@class="ml-dynamic-data"]`)}
    private get $searchBar() {return $(`//input[@id="navsearchbox"]`)}
    private get $searchButton() {return $(`//button[@id="searchBtn"]`)}
    private get $verifySearch() {return $(`//li[@class="active"]`)}
    private get $myAccountButton() {return $('//span[@class="ml-topnav-identity-link"]')}
    private get $usernameField() {return $('//input[@id="loginEmail"]')}
    private get $passwordField() {return $('//input[@id="loginPassword2"]')}
    private get $logInButton() {return $('//input[@id="btnContinueAccountSetupFormLogin"]')}
    private get $toShoppingCart() {return $('//div[@class="popDownNav ml-header-global-cart"]//a[@href="/basket.do?gc=1"]')}


    async navigateTo() {        
        await this.setUrlWaitUntilDisplayed('./', await this.$searchBar);
        //const pageTitle = await this.getText(await this.$pageTitle);
        //await expect(await this.getText(await this.$pageTitle) === await pageTitle);
    }
    
    async moveToCart() {
        await this.clickAndWaitUntilDisplayed(await this.$toShoppingCart, await this.$searchBar, 3000);
    }

    async setSearchValue(productID: string) {
        await this.setValue(await this.$searchBar, productID );
        await this.clickAndWaitUntilDisplayed(await this.$searchButton, await this.$verifySearch);
    }

    async logIn(Username: string, Password: string) {
        await this.clickAndWaitUntilDisplayed(await this.$myAccountButton, await this.$usernameField);
        await this.setValue(await this.$usernameField, Username);
        await this.setValue(await this.$passwordField, Password);
        await this.clickAndWaitUntilNotDisplayed(await this.$logInButton, await this.$usernameField);
    }
}