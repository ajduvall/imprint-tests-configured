import { WdioHelper } from '../helpers/wdioHelper';

export class ProductPage extends WdioHelper {

    private get $securityLevelDropDown() {return $(`//label[contains(text(), "Security Level")]//following::select[1]`)}
    private get $selectBackgroundDropDown() {return $(`//label[contains(text(), "Select Background")]//following::select[1]`)}
    private get $selectAColorCheckDropDown() {return $(`//label[contains(text(), "Select a color")]//following::select[1]`)}
    private get $selectAColorNonCheckDropDown() {return $(`//label[contains(text(), "Select a color")]//following::select[1]`)}
    private get $selectPersonalization() {return $(`//label[contains(text(), "Select Personalization")]//following::select[1]`)}
    private get $enterQuantityBox() {return $(`//div[@id="quantityMsg"]//following::input[1]`)}
    private get $addToCart() {return $(`//div[@id="quantityMsg"]//following::input[2]`)}
    private get $toShoppingCart() {return $('span.ml-header-global-cart-label')}

    //div[@tabindex="-1"]//following::input[@class="JSTextfield"][1]

    // async setCheckDropDowns() {
    //     await this.clickAndWaitUntilClickable(await this.$securityLevelDropDown, await this.$);
    // }

    async setCheckDropDowns() {        
        await this.selectByIndex(await this.$securityLevelDropDown, 1);
        await this.selectByIndex(await this.$selectBackgroundDropDown, 1);
        await this.selectByIndex(await this.$selectAColorCheckDropDown, 1);         
    }

    async setNonCheckDropDowns() {        
        if (await this.isDisplayed(await this.$selectAColorNonCheckDropDown) === true) {
            await this.selectByIndex(await this.$selectAColorNonCheckDropDown, 1);
        }
        else {};
        if (await this.isDisplayed(await this.$selectPersonalization) === true) {
            await this.selectByIndex(await this.$selectPersonalization, 1);
        }
        else {};      
    }

    async setQuantity(quantity: string) {
        await this.setValue(await this.$enterQuantityBox, quantity);
    }

    async addToCart() {
        await this.clickAndWaitUntilDisplayed(await this.$addToCart, await this.$addToCart);
        if (await this.isDisplayed(await this.$addToCart) === false) {
            browser.saveScreenshot(`C:\_git\imprint-tests-configured\Screenshots`);
        }
    }

    async moveToCart() {
        await this.clickAndWaitUntilNotDisplayed(await this.$toShoppingCart, await this.$addToCart, 3000);
    }




}


