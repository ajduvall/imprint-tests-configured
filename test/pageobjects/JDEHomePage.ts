import { WdioHelper } from '../helpers/wdioHelper';

export class JDEHomePage extends WdioHelper {

    //iframe
    private get $newIFrame() {return $(`//iframe[@id="e1menuAppIframe"]`)}

    private get $jdeHomePageImg() {return $(`//img[@id="oracleImage"]`)}
    private get $userIDField() {return $(`//input[@id="User"]`)}
    private get $passwordField() {return $(`//input[@id="Password"]`)}
    private get $signInButton() {return $(`//input[@value="Sign In"]`)}
    private get $openSalesOrderButton() {return $(`//td[contains(text(), "Sales Orders (SO) - No Templates")]`)}
    private get $orderSaveAndContinueButton() {return $(`//button[@id="C0_20"]`)}
    private get $itemNumberField() {return $(`/html/body/form[3]/div/table/tbody/tr/td/div/span[16]/span/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/span/div/span[1]/table/tbody/tr/td/div/span[13]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td/div/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr/td[3]/div/input`)}
    private get $quantityOrderedField() {return $(`//td[@colindex="2"]//div[@tabindex="-1"]//input`)}
    private get $createOrderButton() {return $(`//input[@id="btnCreateOrder"]`)}
    private get $clickHere() {return $(`//a[@target="_blank"]`)}
    private get $imprintOKBoxButton() {return $(`//button[@id="C0_13"]`)}
    private get $imprintIntentBoxButton() {return $(`//button[@id="C0_16"]`)}
    private get $webHistoryFailed() {return $(`//body[@id="t"]`)}
    private get $blockingDiv() {return $(`//div[@id="popupWindowTitlemodalForm0"]`)}
    private get $test() {return $(`/html/body/form[3]/table[2]/tbody/tr/td/table/tbody/tr/td[5]/a/img`)}

    async navigateToJDEDEV() {        
        await this.setUrlWaitUntilDisplayed('./', await this.$userIDField);
    }

    async switchFrame() {
        await this.switchToFrame(await this.$newIFrame);
    }

    async logIn(userid: string, password: string) {
        await this.setValue(await this.$userIDField, userid);
        await this.setValue(await this.$passwordField, password);
        await this.clickAndWaitUntilDisplayed(await this.$signInButton, await this.$openSalesOrderButton);
    }

    async createSalesOrder() { //customer number 1294124 hard coded here. Only works in DEV
        await this.isDisplayed(await this.$openSalesOrderButton);

        await browser.pause(2000);
        await browser.keys(['\uE00A', '\uE009', 'a']);
        //await this.isDisplayed(await this.$soldToField);
        await browser.pause(2000);
        //await this.doubleClick(await this.$soldToField);
        await browser.keys("1294124");
        await browser.keys("\uE004");
        await browser.keys("2");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("1294124");
        await browser.keys(['\uE00A', '\uE009', 'e']);
    }

    async inputFirstOrderLine() {
        await browser.pause(2000);
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys("\uE004");
    }

    async inputNewOrderLine(productID: string, quantity: string) {
        await browser.keys(productID);
        await browser.keys("\uE004");
        await browser.keys("\uE004");
        await browser.keys(quantity);
        await browser.keys("\uE007");
        await browser.pause(3000);
    }

    async inputNewOrderLineWithElements(productID: string, quantity: string) {
        // const selectorAppear = await this.isDisplayed(await this.$itemNumberField);
        // console.log(selectorAppear);
        // // await this.doubleClick(await this.$itemNumberField);
        await this.setValue(await this.$itemNumberField, productID);
        await this.isDisplayed(await this.$quantityOrderedField);
        await this.setValue(await this.$quantityOrderedField, quantity);
        await browser.keys("\uE007");
    }

    async openImprintFromWebImprintHistory() { 
    if (await this.isDisplayed(await this.$webHistoryFailed) === true) {
        await console.error("Web Imprint History page says Site Can't Be Reached");
        await browser.closeWindow();
    }
    else {
        await this.clickAndWaitUntilDisplayed(await this.$createOrderButton, await this.$clickHere);
    await this.clickAndWaitUntilDisplayed(await this.$clickHere, await this.$clickHere);
        }
    }

    async navigateBackToOrder() {
        await browser.keys(['\uE00A', '\uE009', 'o']);
        await browser.pause(3000);
        await browser.keys(['\uE00A', '\uE009', 'o']);
    }


}