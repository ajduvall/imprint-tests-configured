import { WdioHelper } from '../../helpers/wdioHelper';
import { FormHelper } from '../../helpers/FormHelper'
export class Approval extends WdioHelper {

    private get $approvalOpenArrow() {return $('//uib-accordion//span[contains(text(),"Approval")]')};
    private get $approvalButton() {return $('//a[@id="addToCartBtn2"]')}
    private get $initialsBox() {return $('//input[@id="ff_blnApprove"]')}
    private get $approvalPopUp() {return $('//md-dialog')}
    private get $approvalPopUpButton() {return $(`//md-dialog[@class="_md md-transition-in"]//md-dialog-actions[@class="layout-row"]//button[@class="btn btn-primary ng-binding"]`)}
    private get $approvalOKButton() {return $(`//button[contains(text(),"OK")]`)}

    async open() {
        await this.clickAndWaitUntilDisplayed(await this.$approvalOpenArrow, await this.$initialsBox);
    }

    async addInitials(threeletterinitials: string) {
        await this.setValue(await this.$initialsBox, threeletterinitials);
    }
    
    async approveOrderExpectError() {
        await this.clickAndWaitUntilDisplayed(await this.$approvalButton, await this.$approvalPopUpButton);
    }
    
    async approveOrder() {
        await this.clickAndWaitUntilDisplayed(await this.$approvalButton, await this.$approvalOKButton);
        if (await this.isDisplayed(await this.$approvalOKButton) === true) {
            await this.clickAndOnlyClick(await this.$approvalOKButton);
        }
        else if (await this.isDisplayed(await this.$approvalOKButton) === false) {
            await this.clickAndWaitUntilDisplayed(await this.$approvalButton, await this.$approvalOKButton);
            await this.clickAndOnlyClick(await this.$approvalOKButton);
        }
        else {};
        
    }

    async approveOrderCloseWindow() {
        const handle = browser.getWindowHandle();
        await this.clickAndWaitUntilDisplayed(await this.$approvalButton, await this.$approvalOKButton, 30000);
        if (await this.isDisplayed(await this.$approvalOKButton) === true) {
            await this.clickAndOnlyClick(await this.$approvalOKButton);
        }
        else {
            if (browser.getWindowHandle() == handle) {
                await browser.closeWindow()
            }
            else { }
        }
        
    }

    async approveOrderCloseTab() {
        await this.waitUntilDisplayed(await this.$approvalButton);
        await this.clickAndWaitUntilDisplayed(await this.$approvalButton, await this.$approvalOKButton, 30000);
        if (await this.isDisplayed(await this.$approvalOKButton) === true) {
            await this.clickAndOnlyClick(await this.$approvalOKButton);
        }
        else {
            await this.clickAndWaitUntilDisplayed(await this.$approvalButton, await this.$approvalOKButton, 30000);
            await this.clickAndOnlyClick(await this.$approvalOKButton);
        }
    }

    async approveOrderTwo() {
        await this.clickAndWaitUntilDisplayed(await this.$approvalButton, await this.$approvalOKButton, 30000);
        await this.clickAndWaitUntilNotDisplayed(await this.$approvalOKButton, await this.$approvalOKButton);
    }


}