import { WdioHelper } from '../../helpers/wdioHelper';
import { FormHelper } from '../../helpers/FormHelper'
export class CompanyInformation extends WdioHelper {

    private get $companyInformationOpenArrow() {return $('//uib-accordion//span[contains(text(),"Company Information")]')}
    private get $addressBlockField() {return $('//div[@role="textbox"]')}
    private get $helpMeButton() {return $('#CompanyHints')}
    private get $customerServiceRequestBox() {return $(`//label[@id="label_ff_chkHelpMessage"]`)}
    private get $selectStockLogoButton() {return $(`//button[@id="ff_imgSelectLogo_select"]`)}
    private get $selectCustomLogoButton() {return $(`//button[@class="btn btn-primary btn-sm gutter-top-15 AddFileButton AddFileButtonimgLogo"]`)}
    private get $selectFirstLogo() {return $(`//ul[@id="imageContainer"]//li[1]`)}
    private get $useSelectionLogoButton() {return $(`//button[@class="btn btn-primary ng-binding"]`)}
    private get $previewLoading() {return $(`//div[@class="row preview-loader ng-scope"]`)}
    private get $previewLoaded() {return $(`//body[@title="Click to zoom in"]`)}
    private get $preview() {return $(`//div[@style="height: 349.807px; overflow: hidden; width: 269.969px;"]`)} //Preview size is hard coded here. If the window size changes, it will break

    async open() {
        
        if (await this.isDisplayed(await this.$companyInformationOpenArrow) === true) {
            await this.clickAndWaitUntilDisplayed(await this.$companyInformationOpenArrow, await this.$helpMeButton);
        }
        else {
            console.log("No Company Information tab available");
        }
    }

    //Set Fields
    
    async setAddressBlockValue(addressBlockText: string) {
        await this.setValue(await this.$addressBlockField, addressBlockText);
        await this.isDisplayed(await this.$previewLoading); 
        await this.isDisplayed(await this.$previewLoaded);    
    }

    async checkCustomerServiceRequestBox() {
        await this.clickAndOnlyClick(await this.$customerServiceRequestBox);
    }

    async selectStockLogo() {
        const previewSize = await this.getImgSize(await this.$preview)
        await this.clickAndWaitUntilDisplayed(await this.$selectStockLogoButton, await this.$selectFirstLogo);
        await this.clickAndWaitUntilDisplayed(await this.$selectFirstLogo, await this.$useSelectionLogoButton);
        await this.clickAndWaitUntilDisplayed(await this.$useSelectionLogoButton, await this.$selectStockLogoButton);
        await this.waitUntilNotDisplayed(await this.$preview)
        await this.waitUntilDisplayed(await this.$preview)
    }

    //Set Fields If Displayed

    async ifDisplayedSetAddressBlockValue(addressBlockText: string) {
        if (await this.isDisplayed(await this.$addressBlockField) === true) {
            await this.setValue(await this.$addressBlockField, addressBlockText)
        }
        else {
            console.log(`'No Address Block for' ++ ${addressBlockText}`)
        } 
        await this.isDisplayed(await this.$previewLoading); 
        await this.isDisplayed(await this.$previewLoaded);    
    }

    async ifDisplayedSelectStockLogo() {
        if (await this.isDisplayed(await this.$selectStockLogoButton) === true) {
            const previewSize = await this.getImgSize(await this.$preview)
            await this.clickAndWaitUntilDisplayed(await this.$selectStockLogoButton, await this.$selectFirstLogo);
            await this.clickAndWaitUntilDisplayed(await this.$selectFirstLogo, await this.$useSelectionLogoButton);
            await this.clickAndWaitUntilDisplayed(await this.$useSelectionLogoButton, await this.$selectStockLogoButton);
            await this.waitUntilNotDisplayed(await this.$preview);
            await this.waitUntilDisplayed(await this.$preview);
        }
        else {
            console.log("No ability to select logos on this product");
        }
    }
}
// Check Address Block:    /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[3]/div/div
// NonCheck Address Block: /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[5]/div/div
// NewCheck Address Block: /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[3]/div/div
// Postcard Address Block: /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[5]/div/div
// Postcard AddressBlock2: /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[3]/div[2]/div/div/div[3]/div/div