import { WdioHelper } from '../../helpers/wdioHelper';
import { FormHelper } from '../../helpers/FormHelper'
export class SignatureArea extends WdioHelper {

    private get $signatureAreaOpenArrow() {return $(`//uib-accordion//span[contains(text(),"Signature Area")]`)}
    private get $signatureLinesZero() {return $(`//label[text()="0"]//preceding::input[1]`)}
    private get $signatureLinesOne() {return $(`//label[text()="1"]//preceding::input[1]`)}
    private get $signatureLinesTwo() {return $(`//label[text()="2"]//preceding::input[1]`)}
    private get $signatureLinesThree() {return $(`//label[text()="3"]//preceding::input[1]`)}
    private get $signatureLineOneLabel() {return $(`//label[contains(text(),"Signature Line 1 Label")]//following::input[1]`)}
    private get $signatureLineTwoLabel() {return $(`//label[contains(text(),"Signature Line 2 Label")]//following::input[1]`)}
    private get $signatureLineThreeLabel() {return $(`//label[contains(text(),"Signature Line 3 Label")]//following::input[1]`)}
    private get $signatureHeadingLineOne() {return $(`//label[contains(text(),"Signature Heading Line 1")]//following::input[1]`)}
    private get $signatureHeadingLineTwo() {return $(`//label[contains(text(),"Signature Heading Line 2")]//following::input[1]`)}

    async open() {
        
        if (await this.isDisplayed(await this.$signatureAreaOpenArrow)) {
            await this.clickAndWaitUntilDisplayed(await this.$signatureAreaOpenArrow, await this.$signatureAreaOpenArrow);
        }
        else {
            console.log("No Signature Tab available");
        }
    }
    
    //Set Fields

    async setNumberofSignatureLines(value: number){
        if (value === 0) {
            await this.doubleClick(await this.$signatureLinesZero);
        }
        else if (value === 1) {
            await this.doubleClick(await this.$signatureLinesOne);
        }
        else if (value === 2) {
            await this.doubleClick(await this.$signatureLinesTwo);
        }
        else if (value === 3) {
            await this.doubleClick(await this.$signatureLinesThree);
        }
        else {
            console.error("No number of signature lines selected");
        }
    }

    async setSignatureLineOneLabel(label: string) {
        await this.setValue(await this.$signatureLineOneLabel, label);
    }
    
    async setSignatureLineTwoLabel(label: string) {
        await this.setValue(await this.$signatureLineTwoLabel, label);
    }

    async setSignatureLineThreeLabel(label: string) {
        await this.setValue(await this.$signatureLineThreeLabel, label);
    }

    async setSignatureHeadingOneLabel(label: string) {
        await this.setValue(await this.$signatureHeadingLineOne, label);
    }

    async setSignatureHeadingTwoLabel(label: string) {
        await this.setValue(await this.$signatureHeadingLineTwo, label);
    }

    //If Displayed Set Fields

    async ifDisplayedSetNumberofSignatureLines(value: number){
        if (await this.isDisplayed(await this.$signatureLinesZero || await this.$signatureLinesOne || await this.$signatureLinesTwo || await this.$signatureLinesThree)) {
            if (value === 0) {
                await this.doubleClick(await this.$signatureLinesZero);
            }
            else if (value === 1) {
                await this.doubleClick(await this.$signatureLinesOne);
            }
            else if (value === 2) {
                await this.doubleClick(await this.$signatureLinesTwo);
            }
            else if (value === 3) {
                await this.doubleClick(await this.$signatureLinesThree);
            }
            else {
                console.error("No number of signature lines selected");
            }
        }
        else {
            console.log("No display to set number of signature lines");
        }
    }

    async ifDisplayedSetSignatureLineOneLabel(label: string) {
        if (await this.isDisplayed(await this.$signatureLineOneLabel)) {
            await this.setValue(await this.$signatureLineOneLabel, label);
        }
        else {
            console.log("No signature line one label area");
        }
    }

    async ifDisplayedSetSignatureLineTwoLabel(label: string) {
        if (await this.isDisplayed(await this.$signatureLineTwoLabel)) {
            await this.setValue(await this.$signatureLineTwoLabel, label);
        }
        else {
            console.log("No signature line two label area");
        }
    }

    async ifDisplayedSetSignatureLineThreeLabel(label: string) {
        if (await this.isDisplayed(await this.$signatureLineThreeLabel)) {
            await this.setValue(await this.$signatureLineThreeLabel, label);
        }
        else {
            console.log("No signature line three label area");
        }
    }

    async ifDisplayedSetSignatureHeadingOneLabel(label: string) {
        if (await this.isDisplayed(await this.$signatureHeadingLineOne)) {
            await this.setValue(await this.$signatureHeadingLineOne, label);
        }
        else {
            console.log("No signature heading one label area");
        }
    }

    async ifDisplayedSetSignatureHeadingTwoLabel(label: string) {
        if (await this.isDisplayed(await this.$signatureHeadingLineTwo)) {
            await this.setValue(await this.$signatureHeadingLineTwo, label);
        }
        else {
            console.log("No signature heading two label area");
        }
    }
    
}