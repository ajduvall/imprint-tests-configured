import { WdioHelper } from '../../helpers/wdioHelper';
import { FormHelper } from '../../helpers/FormHelper'
export class ProductSelection extends WdioHelper {

    private get $productSelectionOpenArrow() {return $('//uib-accordion//span[contains(text(),"Company Information")]')}
    private get $chooseSwatchButton() {return $("//div[@id='ff_htmlSwatchPicker']//input")}
    private get $goodRadioButton() {return $(`//tr[@id='securityRow']//table//td//label[text()="GOOD"]`)}
    private get $betterRadioButton() {return $(`//tr[@id='securityRow']//table//td//label[text()="BETTER"]//preceding::input[1]`)}
    private get $bestRadioButton() {return $(`//tr[@id='securityRow']//table//td//label[text()="BEST"]//preceding::input[1]`)}
    //private get $goodRadioButton() {return $(`//tr[@id='securityRow']//table//td//input[@value="LOW"]`)}
    private get $goodSwatchOption() {return $(`//tr//td[@id="GalleryContainer"]//div[@id="ImageGallery"]//input[@title="BURGUNDY-MARBLE (80762)"]`)}
    private get $changeQuantityField() {return $(`//div[@id="group_1_Part_1-formFields"]//div[@class="form-group"]//div[@class="default"]//input[@role="textbox"]`)}
    private get $alertPopUpButton() {return $(`//md-dialog[@class="_md md-transition-in"]//md-dialog-actions[@class="layout-row"]//button[@class="btn btn-primary ng-binding"]`)}
    

    async open() {
        await this.clickAndWaitUntilDisplayed(await this.$productSelectionOpenArrow, await this.$chooseSwatchButton);
    }

    //unable to select the swatch or radio button. Radio button does not error, but the swatch does.
    async chooseSwatch() {
        await this.clickAndWaitUntilDisplayed(await this.$chooseSwatchButton, await this.$chooseSwatchButton);
        await browser.pause(5000);
        await this.doubleClick(await this.$betterRadioButton);
        await this.doubleClick(await this.$betterRadioButton);
        await this.clickAndWaitUntilNotDisplayed(await this.$goodSwatchOption, await this.$goodRadioButton);
        await browser.pause(5000);
    }

    async setQuantity(quantity: string) {;
        await this.setValue(await this.$changeQuantityField, quantity);
        await this.clickAndWaitUntilNotDisplayed(await this.$alertPopUpButton, await this.$alertPopUpButton);
    }
}