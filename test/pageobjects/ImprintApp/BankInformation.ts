import { WdioHelper } from '../../helpers/wdioHelper';
import { FormHelper } from '../../helpers/FormHelper'
export class BankInformation extends WdioHelper {

    private get $swatchSecurityLevelBetter() {return $('#rblSecurity_2')}
    private get $bankInformationOpenArrow() {return $('//uib-accordion//span[contains(text(),"Bank Information")]')}
    private get $bankRoutingNumberField() {return $(`//input[@id="ff_strMICRTRN"]`)}
    private get $bankAccountNumberField() {return $(`//input[@id="ff_strMICRTACCT"]`)}
    private get $helpMeBankInformation() {return $(`//span[@id="BankHints"]`)}
    private get $bankNameField() {return $(`//input[@id="ff_strBankLine1"]`)}
    private get $bankAddressLineOneField() {return $(`//label[contains(text(), "Address Line 1:")]//following::input[1]`)}
    private get $bankAddressLineTwoField() {return $(`//label[contains(text(), "Address Line 2:")]//following::input[1]`)}
    private get $bankAddressLineThreeField() {return $(`//label[contains(text(), "Address Line 3:")]//following::input[1]`)}
    private get $bankFractionField() {return $(`//label[contains(text(), "Bank Fraction:")]//following::input[1]`)}
    private get $startingCheckNumberField() {return $(`//label[contains(text(), "Starting Check Number")]//following::input[1]`)}
    private get $reverseNumber() {return $(`//input[@name="blnReverseNumbering"]`)}
    private get $removeNumberOnTopStub() {return $(`//label[contains(text(),"Remove check number on top stub")]//preceding::input[1]`)}
    private get $removeNumberOnBottomStub() {return $(`//label[contains(text(),"Remove check number on bottom stub")]//preceding::input[1]`)}
    //private get $removeNumberOnMiddleStub() {return $(``)}    
    private get $removeNumberOnChecklStub() {return $(`//label[contains(text(),"Remove check number on check")]//preceding::input[1]`)}
    private get $showHideMICRLine() {return $$(`//div[@id="ff_atmMICRLine_chosen"]//div[@class="chosen-drop"]//ul[@class="chosen-results"]`)}
    public get $invalidBankInfoPopUp() {return $(`//div[@class="md-dialog-content ng-binding"]`)}
    public get $invalidBankInfoPopUpAtApproval() {return $(`//div[@class="md-dialog-content ng-binding"][contains(text(),"Before approving your document, you must provide a valid RTN, Account Number, and Approval Initials.")]`)}
    public get expectedAccountNumberErrorText() {return ("You entered invalid characters in your account number and they should be removed.")}
    private get $selectShowHideMICRLine() {return $(`//div[@id="ff_atmMICRLine_chosen"]`)}
    private get $printMICRLine() {return $(`//ul[@class="chosen-results"]//li[@data-option-array-index="0"]`)}
    private get $dontPrintMICRLine() {return $(`//ul[@class="chosen-results"]//li[@data-option-array-index="1"]`)}

    async open() {
        await this.clickAndWaitUntilDisplayed(await this.$bankInformationOpenArrow, await this.$bankInformationOpenArrow);
        // if (await this.isDisplayed(await this.$helpMeBankInformation || this.$bankRoutingNumberField || this.$bankAccountNumberField)) { return console.log("Bank Information Opened!") }
        // else {
        //     this.clickAndWaitUntilDisplayed(await this.$bankInformationOpenArrow, await this.$helpMeBankInformation)
        // }
    }

    //Set Field
    
    async inputValidBankInformation() {
        await this.isDisplayed(await this.$bankRoutingNumberField);
        await this.setValue(await this.$bankRoutingNumberField, "091000022");
        await this.isDisplayed(await this.$bankAccountNumberField);
        await this.setValue(await this.$bankAccountNumberField, "x123456789o");
    }

    async inputInvalidBankInformation(bankRtn: string, bankAcct: string) {
        await this.setValue(await this.$bankRoutingNumberField, bankRtn);
        await this.setValue(await this.$bankAccountNumberField, bankAcct);
        await this.clickAndOnlyClick(await this.$startingCheckNumberField);
    }

    async inputBankAddress(addressLineOne: string, addressLineTwo: string, addressLineThree: string) {
        await this.setValue(await this.$bankAddressLineOneField, addressLineOne);
        await this.setValue(await this.$bankAddressLineTwoField, addressLineTwo);
        await this.setValue(await this.$bankAddressLineThreeField, addressLineThree);
    }

    async isAlertDisplayed() {
        return this.isDisplayed(await this.$invalidBankInfoPopUp);
    }

    async isAlertDisplayedAtApproval() {
        return this.isDisplayed(await this.$invalidBankInfoPopUpAtApproval);
    }

    async getAlertText() {
        await this.getText(await this.$invalidBankInfoPopUp);
    }

    async setBankFraction(bankFraction: string) {
        await this.setValue(await this.$bankFractionField, bankFraction);
    }

    async setStartingCheckNumber(startingCheckNumber: string) {
        await this.setValue(await this.$startingCheckNumberField, startingCheckNumber);
    }

    async checkReverseNumber() {
        await this.clickAndOnlyClick(await this.$reverseNumber);
    }

    async checkRemoveCheckNumberTopStub() {
        await this.clickAndOnlyClick(await this.$removeNumberOnTopStub);
    }

    async checkRemoveCheckNumberBottomStub() {
        await this.clickAndOnlyClick(await this.$removeNumberOnBottomStub);
    }

    async checkRemoveCheckNumberonCheck() {
        await this.clickAndOnlyClick(await this.$removeNumberOnChecklStub);
    }

    async checkForBankInformationError() {
        return await this.isDisplayed(await this.$invalidBankInfoPopUp);
    }

    async setShowMICRLine() {
        await this.clickAndWaitUntilDisplayed(await this.$selectShowHideMICRLine, await this.$selectShowHideMICRLine);
        await this.clickAndWaitUntilDisplayed(await this.$printMICRLine, await this.$printMICRLine);
    }

    async setHideMICRLine() {
        await this.clickAndWaitUntilDisplayed(await this.$selectShowHideMICRLine, await this.$selectShowHideMICRLine);
        await this.clickAndWaitUntilDisplayed(await this.$dontPrintMICRLine, await this.$dontPrintMICRLine);
    }

    //If Displayed Set Field

    async ifDisplayedSetStartingCheckNumber(startingCheckNumber: string) {
        if (await this.isDisplayed(await this.$startingCheckNumberField) === true) {
            await this.setValue(await this.$startingCheckNumberField, startingCheckNumber);
        }
        else {
            console.log("No box to set starting check number");
        }
    }

    async isDisplayedOpen() {
        const toBeWithBankInfoTab = [await this.$startingCheckNumberField, await this.$bankRoutingNumberField, await this.$bankAccountNumberField]
        if (await this.isDisplayed(await this.$bankInformationOpenArrow) === true) {
            await this.clickAndWaitUntilAnyDisplay(await this.$bankInformationOpenArrow, await toBeWithBankInfoTab);
        }
        else {
            console.log("No Bank Information tab")
        };
    }
    
    async isDisplayedInputValidBankInformation() {
        if (await this.isDisplayed(await this.$bankRoutingNumberField) === true) {
            await this.setValue(await this.$bankRoutingNumberField, "091000022");
        }
        else {
            console.log("No Routing Number");
        };
        if (await this.isDisplayed(await this.$bankAccountNumberField) === true) {
            await this.setValue(await this.$bankAccountNumberField, "x123456789o");
        }
        else {
            console.log("No Account Number");
        };
    }
}

