import { WdioHelper } from '../../helpers/wdioHelper';
import { FormHelper } from '../../helpers/FormHelper'
export class VoucherDetails extends WdioHelper {

    private get $openVoucherLine() {return $(`//uib-accordion//span[contains(text(),"Voucher Details")]`)}
    private get $voucherLineOne() {return $(`//input[@id="ff_strVoucherLine"]`)}
    public get $voucherLineTwo() {return $(`//input[@id="ff_strVoucherLine2"]`)}
    private get $removeVoucherLineTopStub() {return $(`//label[contains(text(),"Remove voucher line on top stub")]//preceding::input[1]`)}
    private get $removeVoucherLineBottomStub() {return $(`//label[contains(text(),"Remove voucher line on bottom stub")]//preceding::input[1]`)}
    private get $voucherAlignmentLeft() {return $(`//label[contains(text(),"Left")]//preceding::input[1]`)}
    private get $voucherAlignmentCenter() {return $(`//label[contains(text(),"Center")]//preceding::input[1]`)}
    private get $voucherAlignmentRight() {return $(`//label[contains(text(),"Right")]//preceding::input[1]`)}

    async open() {
        if (await this.isDisplayed(await this.$openVoucherLine) === true) {
            await this.clickAndWaitUntilDisplayed(await this.$openVoucherLine, await this.$voucherLineOne);
        }
        else {
            console.log("No Voucher Details tab")
        };
    }
    
    //Set Fields
    
    async setVoucherLineOne(voucherLine: string) {
        await this.setValue(await this.$voucherLineOne, voucherLine);
    }

    async setVoucherLineTwo(voucherLine: string) {
        await this.setValue(await this.$voucherLineTwo, voucherLine);
    }

    async removeVoucherOnTopStub(voucherLine: string) {
        await this.clickAndOnlyClick(await this.$removeVoucherLineTopStub);
    }

    async removeVoucherOnBottomStub(voucherLine: string) {
        await this.clickAndOnlyClick(await this.$removeVoucherLineBottomStub);
    }

    async setVoucherAlignmentLeft() {
        await this.clickAndOnlyClick(await this.$voucherAlignmentLeft);
    }

    async setVoucherAlignmentRight() {
        await this.clickAndOnlyClick(await this.$voucherAlignmentRight);
    }

    async setVoucherAlignmentCenter() {
        await this.clickAndOnlyClick(await this.$voucherAlignmentCenter);
    }

    //Set Fields If Displayed

    async ifDisplayedRemoveVoucherLineOnTopStub() {
        if (await this.isDisplayed(await this.$removeVoucherLineTopStub) === true) {
            await this.clickAndOnlyClick(await this.$removeVoucherLineTopStub);
        }
        else {
            console.log("No box to remove voucher on top stub");
        }
    }

    async ifDisplayedRemoveVoucherLineOnBottomStub() {
        if (await this.isDisplayed(await this.$removeVoucherLineBottomStub) === true) {
            await this.clickAndOnlyClick(await this.$removeVoucherLineBottomStub);
        }
        else {
            console.log("No box to remove voucher on bottom stub");
        }
    }
    
    async ifDisplayedSetVoucherLineOne(voucherLine: string) {
        if (await this.isDisplayed(await this.$voucherLineOne) === true) {
            await this.setValue(await this.$voucherLineOne, voucherLine);
        }
        else {
            console.log("No Voucher Line One Displayed");
        }
    }
    
    async ifDisplayedSetVoucherLineTwo(voucherLine: string) {
        if (await this.isDisplayed(await this.$voucherLineTwo) === true) {
            await this.setValue(await this.$voucherLineTwo, voucherLine);
        }
        else {
            console.log("No Voucher Line Two Displayed");
        }
    }
}