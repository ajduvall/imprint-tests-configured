import { WdioHelper } from '../../helpers/wdioHelper';
import { BaseJDE } from './BaseJDE';

export class CustomerInfoJDE extends BaseJDE {

    private get $addOrderButton() {return $(`//img[@id="hc_Add"]`)} //from page before the customer info
    private get $soldToField() {return $(`//input[@id="C0_18_51"]`)}
    private get $soldToContactIDField() {return $(`//input[@id="C0_18_147"]`)}
    private get $shipToField() {return $(`//input[@id="C0_18_48"]`)}
    private get $saveAndContinueButton() {return $(`//button[@id="C0_20"]`)}
    private get $quantityOrderedField() {return $(`//td[@colindex="2"]//div[@tabindex="-1"]//input[@class="JSTextfield"]`)}

    
    async selectSalesOrderTemplate() {
        await this.navJDE.clickSalesOrder();
    }

    async navigateTo() {
        await this.switchToIFrame();
        await this.waitUntilDisplayed(await this.$addOrderButton);
        await this.clickAndWaitUntilDisplayed(await this.$addOrderButton, await this.$soldToField);
        await this.clearIFrame();
    }

    async addCustomerInfo(customerID: string, contactIDNumber: string) {
        await this.switchToIFrame();
        await this.setValue(await this.$soldToField, customerID);
        await this.setValue(await this.$soldToContactIDField, contactIDNumber);
        await this.setValue(await this.$shipToField, customerID);
        await this.clickAndWaitUntilDisplayed(await this.$saveAndContinueButton, await this.$quantityOrderedField);
        await this.clearIFrame();
    }

    async addNewCustomerInfo(customerID: string, contactIDNumber: string) {
        await this.setValue(await this.$soldToField, customerID);
        await this.setValue(await this.$soldToContactIDField, contactIDNumber);
        await this.setValue(await this.$shipToField, customerID);
    }



}