import { WdioHelper } from '../../helpers/wdioHelper';
import { NavJDE } from './NavJDE';

export abstract class BaseJDE extends WdioHelper {

    public navJDE = new NavJDE(); 
    
    public get $newIFrame() {return $(`//iframe[@id="e1menuAppIframe"]`)}

    async switchToIFrame() {
        await this.switchToFrame(await this.$newIFrame);
    }

    async clearIFrame() {
        await this.switchToFrame(null);
    }

}