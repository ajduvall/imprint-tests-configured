import { WdioHelper } from '../../helpers/wdioHelper';
import { BaseJDE } from './BaseJDE';

export class SalesOrderJDE extends BaseJDE {

    private get $itemNumberFieldPreClick() {return $(`//input[@class="JSTextfield"]`)}
    private get $itemNumberField() {return $(`//input[@tabindex="114"]`)}
    private get $quantityOrderedField() {return $(`//td[@colindex="2"]//div[@tabindex="-1"]//input[@class="JSTextfield"]`)}
    private get $createOrderButton() {return $(`//input[@id="btnCreateOrder"]`)}
    private get $clickHere() {return $(`//a[@target="_blank"]`)}
    private get $imprintOKBoxButton() {return $(`//button[@id="C0_13"]`)}
    private get $imprintIntentBoxButton() {return $(`//button[@id="C0_16"]`)}
    private get $webHistoryFailed() {return $(`//body[@id="t"]`)}
    private get $blockingDiv() {return $(`//div[@id="popupWindowTitlemodalForm0"]`)}



    async inputProductInfo(productID: string, quantity: string) {
        await this.switchToIFrame()
        //await this.clickAndWaitUntilDisplayed(await this.$itemNumberFieldPreClick, await this.$itemNumberField);
        await this.setValue(await this.$itemNumberField, productID);
        await this.setValue(await this.$quantityOrderedField, quantity);
        await this.clearIFrame();
        await browser.keys("\uE007");
    }

    async openImprintFromWebImprintHistory() { 
        if (await this.isDisplayed(await this.$webHistoryFailed) === true) {
            await console.error("Web Imprint History page says Site Can't Be Reached");
            await browser.closeWindow();
        }
        else {
            await this.waitUntilDisplayed(await this.$createOrderButton);
            await this.clickAndWaitUntilDisplayed(await this.$createOrderButton, await this.$clickHere);
            await this.clickAndWaitUntilDisplayed(await this.$clickHere, await this.$clickHere);
            }
        }

}