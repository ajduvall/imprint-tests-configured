import { WdioHelper } from '../../helpers/wdioHelper';
import { BaseJDE } from './BaseJDE';

export class NavJDE extends WdioHelper {

    private get $openSalesOrderButton() {return $(`//td[contains(text(), "Sales Orders (SO) - No Templates")]`)}

    async clickSalesOrder() {
        await this.clickAndWaitUntilDisplayed(await this.$openSalesOrderButton, await this.$openSalesOrderButton);
    }
}

