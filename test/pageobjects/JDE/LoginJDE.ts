import { WdioHelper } from '../../helpers/wdioHelper';
import { BaseJDE } from './BaseJDE';

export class LoginJDE extends WdioHelper {

    private get $userIDField() {return $(`//input[@id="User"]`)}
    private get $passwordField() {return $(`//input[@id="Password"]`)}
    private get $signInButton() {return $(`//input[@value="Sign In"]`)}
    private get $openSalesOrderButton() {return $(`//td[contains(text(), "Sales Orders (SO) - No Templates")]`)}

    async navigateTo() {        
        await this.setUrlWaitUntilDisplayed('./', await this.$userIDField);
    }

    async logIn(userid: string, password: string) {
        await this.setValue(await this.$userIDField, userid);
        await this.setValue(await this.$passwordField, password);
        await this.clickAndWaitUntilDisplayed(await this.$signInButton, await this.$openSalesOrderButton);
    }

}