import { forEachChild } from 'typescript';
import { WdioHelper } from '../helpers/wdioHelper';

export class ShoppingCart extends WdioHelper {

    //private get $pageTitle() {return $(`//title[contains(text(), "Basket")]`)}
    //private get pageTitle() {return this.$pageTitle.getText()}
    private get $viewEditPersonalization() {return $('span.defaultunderline.ml-edit-link')} 
    private get imprintPageTitle() {return ("** STAGED CONTENT ** Customize")}
    private get $removeButton() {return $('//div[@class="ml-basket-item-remove"]//button[@class="ml-secondary-button-understated"]')}
    private get $$removeButtonForAll() {return $$('//button[contains(text(), "Remove")]')}
    private get $shoppingCartTitle() {return $("//head//title")}
    private get $shoppingCartUpdateReloadTrigger() {return $(`//form[@action="/basket.do?method=update&from=Search"]`)}

    async getPageTitle() {
        return this.getText(await this.$shoppingCartTitle);
    }

    async openImprintFromCart() {
        await this.clickAndWaitUntilDisplayed(await this.$viewEditPersonalization, await this.$viewEditPersonalization);
        await browser.switchWindow(await this.imprintPageTitle);
        await this.setWindowSize(1500, 1000);
    }

    async clearCart() {
        if (await this.isDisplayed(await this.$removeButton) === true) {
            await this.clickAndWaitUntilNotDisplayed(await this.$removeButton, await this.$removeButton);
        }
        else {};
    }

    async switchToShoppingCart(pageTitle: string) {
        await this.switchToWindow(pageTitle);
        //await this.switchToWindow("https://greatland-v162-rev.aws.marketlive.com/basket.do?nav=%2Fproduct%2Fid%2F111357&gc=1");
    }

    async clearCartAll() {
        if(await this.isDisplayed(await this.$removeButton)) {
            await this.clickAndWaitUntilNotDisplayed(await this.$removeButton, await this.$shoppingCartUpdateReloadTrigger);
        }
        else {};   
        if(await this.isDisplayed(await this.$removeButton)) {
            await this.clickAndWaitUntilNotDisplayed(await this.$removeButton, await this.$shoppingCartUpdateReloadTrigger);
        }
        else {}; 
        if(await this.isDisplayed(await this.$removeButton)) {
            await this.clickAndWaitUntilNotDisplayed(await this.$removeButton, await this.$shoppingCartUpdateReloadTrigger);
        }
        else {}; 
        if(await this.isDisplayed(await this.$removeButton)) {
            await this.clickAndWaitUntilNotDisplayed(await this.$removeButton, await this.$shoppingCartUpdateReloadTrigger);
        }
        else {}; 
        if(await this.isDisplayed(await this.$removeButton)) {
            await this.clickAndWaitUntilNotDisplayed(await this.$removeButton, await this.$shoppingCartUpdateReloadTrigger);
        }
        else {}; 
        if(await this.isDisplayed(await this.$removeButton)) {
            await this.clickAndWaitUntilNotDisplayed(await this.$removeButton, await this.$shoppingCartUpdateReloadTrigger);
        }
        else {};   
    }
}


    // async generateXpathForCartRemoval() {
    //     const arrayLength
    //     for (let i = 4; i < arrayLength + 3; i++) {
    //         return i
    //     }






    // /html/body/main/div/div/div/div[2]/form/div[3]/div[6]/div/div/div/div[1]/div[4]/div[1]/div[3]/div[2]/div[2]/button
    // /html/body/main/div/div/div/div[2]/form/div[3]/div[5]/div/div/div/div[1]/div[4]/div[1]/div[3]/div[2]/div[2]/button
    // /html/body/main/div/div/div/div[2]/form/div[3]/div[4]/div/div/div/div[1]/div[4]/div[1]/div[3]/div[2]/div[2]/button

    //*[@id="itemtable_container"]/div[4]/div/div/div/div[1]/div[4]/div[1]/div[3]/div[2]/div[2]/button



    //     const imprintDropDownsCount = (await this.$$arrayOfImprintDropDowns).length;