import * as fs from 'fs';
import * as path from 'path';
import * as util from 'util';

export class FileDirectoryHelper {

    async getFilesInFolder(folderPath: string) {
        const absolutePath = await this.getAbsolutePath(folderPath);

        const readdir = util.promisify(fs.readdir);
        const fileList: string[] = await readdir(absolutePath);
        return fileList;
    }

    async getAbsolutePath(relativePath: string) {
        return path.resolve(__dirname, relativePath);
    }
}