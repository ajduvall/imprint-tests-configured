"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FieldType = void 0;
var FieldType;
(function (FieldType) {
    FieldType["Checkbox"] = "checkbox";
    FieldType["Input"] = "input";
    FieldType["Radio"] = "radio";
    FieldType["Select"] = "select";
})(FieldType = exports.FieldType || (exports.FieldType = {}));
