"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WdioHelper = void 0;
var WdioHelper = /** @class */ (function () {
    function WdioHelper() {
    }
    Object.defineProperty(WdioHelper.prototype, "timeoutDefault", {
        get: function () { return 10000; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WdioHelper.prototype, "timeoutImport", {
        get: function () { return 120000; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WdioHelper.prototype, "timeoutSubmitOrder", {
        get: function () { return 60000; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WdioHelper.prototype, "$loadSpinner", {
        get: function () { return $('.loader-backdrop'); },
        enumerable: false,
        configurable: true
    });
    //
    // Browser Actions
    //
    WdioHelper.prototype.setUrlWaitUntilDisplayed = function (url, displayElement, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, browser.url(url)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.waitUntilDisplayed(displayElement)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.setUrlWaitUntilAnyDisplay = function (url, elements, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, browser.url(url)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.waitUntilAnyDisplay(elements)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.getUrl = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, browser.getUrl()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    WdioHelper.prototype.closeBrowser = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, browser.closeWindow()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    //
    // Element Actions
    //
    WdioHelper.prototype.clickAndWaitUntilDisplayed = function (clickElement, displayElement, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.click(clickElement)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.waitUntilDisplayed(displayElement, timeout)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.clickAndWaitUntilNotDisplayed = function (clickElement, displayElement, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.click(clickElement)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.waitUntilNotDisplayed(displayElement, timeout)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.clickAndWaitUntilAnyDisplay = function (clickElement, displayElements, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.click(clickElement)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.waitUntilAnyDisplay(displayElements, timeout)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.clickAndWaitUntilClickable = function (clickElement, clickableElement, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.click(clickElement)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.waitUntilClickable(clickableElement, timeout)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.clickAndWaitUntilNotClickable = function (clickElement, clickableElement, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.click(clickElement)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.waitUntilNotClickable(clickableElement, timeout)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.clickAndOnlyClick = function (clickElement) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.click(clickElement)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.setValue = function (element, value) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.setValue(value)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.addValue = function (element, value) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.addValue(value)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.selectByValue = function (element, value) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.selectByAttribute('value', value)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.selectByIndex = function (element, value) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.selectByIndex(value)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.waitUntilDisplayed = function (element, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.waitForDisplayed({ timeout: timeout })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.waitUntilNotDisplayed = function (element, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.waitForDisplayed({ timeout: timeout, reverse: true })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.waitUntilAnyDisplay = function (elements, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, browser.waitUntil(function () { return __awaiter(_this, void 0, void 0, function () {
                            var elementFound, _i, elements_1, element;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        elementFound = false;
                                        _a.label = 1;
                                    case 1:
                                        if (!(elementFound === false)) return [3 /*break*/, 6];
                                        _i = 0, elements_1 = elements;
                                        _a.label = 2;
                                    case 2:
                                        if (!(_i < elements_1.length)) return [3 /*break*/, 5];
                                        element = elements_1[_i];
                                        return [4 /*yield*/, this.isDisplayed(element)];
                                    case 3:
                                        elementFound = _a.sent();
                                        _a.label = 4;
                                    case 4:
                                        _i++;
                                        return [3 /*break*/, 2];
                                    case 5: return [3 /*break*/, 1];
                                    case 6: return [2 /*return*/, true];
                                }
                            });
                        }); }, { timeout: timeout })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.waitUntilEnabled = function (element, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.waitForEnabled({ timeout: timeout })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.isDisplayed = function (element) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, element.isDisplayed()];
                }
            });
        });
    };
    WdioHelper.prototype.isSelected = function (element) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, element.isSelected()];
                }
            });
        });
    };
    WdioHelper.prototype.waitUntilClickable = function (element, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.waitForClickable({ timeout: timeout })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.waitUntilNotClickable = function (element, timeout) {
        if (timeout === void 0) { timeout = this.timeoutDefault; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.waitForClickable({ timeout: timeout, reverse: true })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WdioHelper.prototype.getAttribute = function (element, attribute) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, element.getAttribute(attribute)];
                }
            });
        });
    };
    WdioHelper.prototype.getText = function (element) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, element.getText()];
                }
            });
        });
    };
    WdioHelper.prototype.getValue = function (element) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, element.getValue()];
                }
            });
        });
    };
    //
    // Private
    //
    WdioHelper.prototype.checkAndWaitLoadSpinner = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    WdioHelper.prototype.click = function (element) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkAndWaitLoadSpinner()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, element.click()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return WdioHelper;
}());
exports.WdioHelper = WdioHelper;
