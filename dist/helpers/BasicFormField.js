"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasicFormField = void 0;
var FormField_1 = require("./FormField");
var FieldType_1 = require("./FieldType");
var BasicFormField = /** @class */ (function (_super) {
    __extends(BasicFormField, _super);
    function BasicFormField(name, type, optionValue) {
        if (type === void 0) { type = FieldType_1.FieldType.Input; }
        var _this = _super.call(this, type, optionValue) || this;
        _this.name = name;
        return _this;
    }
    return BasicFormField;
}(FormField_1.FormField));
exports.BasicFormField = BasicFormField;
