"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FormField = void 0;
var FieldType_1 = require("./FieldType");
var FormField = /** @class */ (function () {
    function FormField(type, optionValue) {
        if (type === void 0) { type = FieldType_1.FieldType.Input; }
        this.type = type;
        this.optionValue = optionValue;
    }
    return FormField;
}());
exports.FormField = FormField;
