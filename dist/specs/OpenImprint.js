"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var HomePage_1 = require("../pageobjects/HomePage");
var ImprintApp_1 = require("../pageobjects/ImprintApp");
var ProductPage_1 = require("../pageobjects/ProductPage");
var ShoppingCart_1 = require("../pageobjects/ShoppingCart");
var homePage = new HomePage_1.HomePage();
var productPage = new ProductPage_1.ProductPage();
var shoppingCart = new ShoppingCart_1.ShoppingCart();
var imprintApp = new ImprintApp_1.ImprintApp();
describe('Imprint should', function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        beforeAll(function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, homePage.navigateTo()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        // it('navigate to Imprint', async () => {
        //     await homePage.navigateTo();
        //     await homePage.setSearchValue();
        //     await productPage.setCheckDropDowns();
        // })
        it('open at some point', function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, homePage.setSearchValue()];
                    case 1:
                        _a.sent();
                        // expect(await productPage.isDisplayed()).toBeTrue;
                        //await productPage.setQuantity();
                        return [4 /*yield*/, productPage.setDropDowns()];
                    case 2:
                        // expect(await productPage.isDisplayed()).toBeTrue;
                        //await productPage.setQuantity();
                        _a.sent();
                        return [4 /*yield*/, productPage.setQuantity()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, productPage.addToCart()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, productPage.moveToCart()];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, shoppingCart.openImprintFromCart()];
                    case 6:
                        _a.sent();
                        // await imprintApp.changeSwatch(); //work in progress
                        return [4 /*yield*/, imprintApp.inputBankInfo("091000022", "x123456789o")];
                    case 7:
                        // await imprintApp.changeSwatch(); //work in progress
                        _a.sent();
                        return [4 /*yield*/, imprintApp.approveOrder("SEL")];
                    case 8:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
