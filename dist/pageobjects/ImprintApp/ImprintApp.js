"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImprintApp = void 0;
var wdioHelper_1 = require("../../helpers/wdioHelper");
var BankInformation_1 = require("./BankInformation");
var Approval_1 = require("./Approval");
var CompanyInformation_1 = require("./CompanyInformation");
var ImprintApp = /** @class */ (function (_super) {
    __extends(ImprintApp, _super);
    function ImprintApp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.bankInformation = new BankInformation_1.BankInformation();
        _this.approval = new Approval_1.Approval();
        _this.companyInformation = new CompanyInformation_1.CompanyInformation();
        return _this;
        // private get $productSelectionDropDown() {return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[1]/div[1]/h4/a/span')}
        // private get $chooseSwatchButton() {return $('#ff_htmlSwatchPicker input[type=button')}
        // private get $swatchWindowLink() {return ('https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=60083&d=84733&pt=c')}
        // private get $newSwatchOption() {return $('#ImageGallery input[title=BURGUNDY-MARBLE (80762)')}
        // //Can these be stored in an array?
        // private get $swatchSecurityLevelGood() {return $('[value="LOW"]')}
        // // private get $swatchSecurityLevelBetter() {return $('#rblSecurity_1')}
        // private get $swatchSecurityLevelBetter() {return $('#rblSecurity_2')}
        //    private get $companyInformationCheckDropDown() {return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[1]/h4/a/span/i[1]')}
        // private get $companyInformationNonCheckDropDown() {return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[1]/h4/a/span/i[1]')}
        // private get $companyHintsLogo() {return $('#CompanyHints')}
        //    private get $addressBlockCheckField() {return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[3]/div/div')}
        // private get $addressBlockNonCheckField() {return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[5]/div/div')}
        // // private get $bankInformationDropDown() {return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[3]/div[1]/h4/a/span')}
        // //    private get $approvalCheckDropDown() {return $(`/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[6]/div[1]/h4/a/span/i[1]`)}
        // // private get $approvalNonCheckDropDown() {return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[3]/div[1]/h4/a/span/i[1]')}
        // private get $initialsBox() {return $('#ff_blnApprove')}
        // private get $allDropDowns() {return $$('h4 .panel-title')}
        // // private get $productSelectionDropDown() {return $('//uib-accordion//span[contains(text(),"Product Selection")]')}
        // // private get $$arrayOfImprintDropDowns() {return $$('div[role=tab]')} 
        // // private get $popUpTrue() {return $('#cke_ff_fmpTTRC div[aria-hidden=true]')}
        // async gotoDropDown(dropDownTitle: string) {
        //     return `//uib-accordion//span[contains(text(),"${dropDownTitle}")]`;
        // }
        // async changeSwatch() {        
        //     await this.clickAndWaitUntilDisplayed(await this.$chooseSwatchButton, await this.$chooseSwatchButton);
        //     //await browser.switchWindow(await this.$swatchWindowLink);
        //     await browser.pause(2000);
        //     if (await this.isSelected(await this.$swatchSecurityLevelGood)) {
        //         await this.clickAndWaitUntilClickable(await this.$swatchSecurityLevelBetter, await this.$swatchSecurityLevelBetter);
        //     }
        //     else {
        //         await this.clickAndWaitUntilClickable(await this.$swatchSecurityLevelGood, await this.$swatchSecurityLevelGood);
        //     }
        //     await browser.pause(3000);
        //     //need to find a way to have the window switch to the swatch window when it opens. Right now the url changes each time (see below)
        // }
        // async inputCompanyInformationCheck(textInput: string) {
        //     await this.clickAndWaitUntilDisplayed(await this.$companyInformationCheckDropDown, await this.$addressBlockCheckField)
        //     await this.addValue(await this.$addressBlockCheckField, textInput);
        // }
        // async inputCompanyInformationNonCheck(textInput: string) {
        //     await this.clickAndWaitUntilDisplayed(await this.$companyInformationNonCheckDropDown, await this.$addressBlockNonCheckField)
        //     await this.setValue(await this.$addressBlockNonCheckField, textInput);
        // }
        // async inputBankInfo(bankRtn: string, bankAcct: string) {        
        //     await this.clickAndWaitUntilDisplayed(await this.$bankInformationDropDown, await this.$bankRoutingNumberField);
        //     await this.setValue(await this.$bankRoutingNumberField, bankRtn);
        //     await this.setValue(await this.$bankAccountNumberField, bankAcct);
        // }
        // async approveCheckOrder(threeletterinitials: string) {
        //     await this.clickAndWaitUntilDisplayed(await this.$approvalCheckDropDown, await this.$approvalButton)
        //     await this.setValue(await this.$initialsBox, threeletterinitials)
        //     await this.clickAndWaitUntilNotDisplayed(await this.$approvalButton, await this.$approvalButton);
        //     //find a way to increase the timeout here, in case imprint does not auto-close like it says (set to a 10 second timeout)
        // }
        // async approveNonCheckOrder(threeletterinitials: string) {
        //     await this.clickAndWaitUntilDisplayed(await this.$approvalNonCheckDropDown, await this.$approvalButton)
        //     await this.setValue(await this.$initialsBox, threeletterinitials)
        //     await this.clickAndWaitUntilNotDisplayed(await this.$approvalButton, await this.$approvalButton);
        //     //find a way to increase the timeout here, in case imprint does not auto-close like it says (set to a 10 second timeout)
        // }
        // async approveOrderUsingDropDownArray() {        
        //     const imprintDropDownsCount = (await this.$$arrayOfImprintDropDowns).length;
        //     const approvalTab: number = imprintDropDownsCount - 1;
        //     const $approvalDropDown = $(`/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[${approvalTab}]/div[1]/h4/a/span/i[1]`);
        //     console.log(approvalTab);
        //     await this.clickAndWaitUntilDisplayed(await $approvalDropDown, await this.$approvalButton);
        //     await browser.pause(5000);
        // }
    }
    return ImprintApp;
}(wdioHelper_1.WdioHelper));
exports.ImprintApp = ImprintApp;
//
// Swatch Picker URL https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=59964&d=84613&pt=c
// Swatch Picker URL https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=59967&d=84616&pt=c
//                   https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=59968&d=84617&pt=c
//ff_htmlSwatchPicker
//acctNumber: string, rtNumber: string,
//i.icon.ng-scope.icon-pf_btn_next_arrow'
// approval drop down arrow xpaths
// noncheck
// /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[3]/div[1]/h4/a/span/i[1]
// check
// /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[6]/div[1]/h4/a/span/i[1]
