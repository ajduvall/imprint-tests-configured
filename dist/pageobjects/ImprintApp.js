"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImprintApp = void 0;
var wdioHelper_1 = require("../helpers/wdioHelper");
var ImprintApp = /** @class */ (function (_super) {
    __extends(ImprintApp, _super);
    function ImprintApp() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ImprintApp.prototype, "$productSelectionDropDown", {
        get: function () { return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[1]/div[1]/h4/a/span'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$chooseSwatchButton", {
        get: function () { return $('#ff_htmlSwatchPicker input[type=button'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$swatchWindowLink", {
        get: function () { return ('https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=60083&d=84733&pt=c'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$newSwatchOption", {
        get: function () { return $('#ImageGallery input[title=BURGUNDY-MARBLE (80762)'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$swatchSecurityLevelGood", {
        //Can these be stored in an array?
        get: function () { return $('[value="LOW"]'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$swatchSecurityLevelBetter", {
        // private get $swatchSecurityLevelBetter() {return $('#rblSecurity_1')}
        get: function () { return $('#rblSecurity_2'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$companyInformationCheckDropDown", {
        get: function () { return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[1]/h4/a/span/i[1]'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$companyInformationNonCheckDropDown", {
        get: function () { return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[1]/h4/a/span/i[1]'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$companyHintsLogo", {
        get: function () { return $('#CompanyHints'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$addressBlockCheckField", {
        get: function () { return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[3]/div/div'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$addressBlockNonCheckField", {
        get: function () { return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[2]/div[2]/div/div/div[5]/div/div'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$bankInformationDropDown", {
        get: function () { return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[3]/div[1]/h4/a/span'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$bankRoutingNumberField", {
        get: function () { return $('#ff_strMICRTRN'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$bankAccountNumberField", {
        get: function () { return $('#ff_strMICRTACCT'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$approvalCheckDropDown", {
        get: function () { return $("/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[6]/div[1]/h4/a/span/i[1]"); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$approvalNonCheckDropDown", {
        get: function () { return $('/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[3]/div[1]/h4/a/span/i[1]'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$approvalButton", {
        get: function () { return $('#addToCartBtn2'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$initialsBox", {
        get: function () { return $('#ff_blnApprove'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$allDropDowns", {
        get: function () { return $$('h4 .panel-title'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$$arrayOfImprintDropDowns", {
        get: function () { return $$('div[role=tab]'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ImprintApp.prototype, "$popUpTrue", {
        get: function () { return $('#cke_ff_fmpTTRC div[aria-hidden=true]'); },
        enumerable: false,
        configurable: true
    });
    ImprintApp.prototype.changeSwatch = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e, _f, _g;
            return __generator(this, function (_h) {
                switch (_h.label) {
                    case 0:
                        _a = this.clickAndWaitUntilDisplayed;
                        return [4 /*yield*/, this.$chooseSwatchButton];
                    case 1:
                        _b = [_h.sent()];
                        return [4 /*yield*/, this.$chooseSwatchButton];
                    case 2: return [4 /*yield*/, _a.apply(this, _b.concat([_h.sent()]))];
                    case 3:
                        _h.sent();
                        //await browser.switchWindow(await this.$swatchWindowLink);
                        return [4 /*yield*/, browser.pause(2000)];
                    case 4:
                        //await browser.switchWindow(await this.$swatchWindowLink);
                        _h.sent();
                        _c = this.isSelected;
                        return [4 /*yield*/, this.$swatchSecurityLevelGood];
                    case 5: return [4 /*yield*/, _c.apply(this, [_h.sent()])];
                    case 6:
                        if (!_h.sent()) return [3 /*break*/, 10];
                        _d = this.clickAndWaitUntilClickable;
                        return [4 /*yield*/, this.$swatchSecurityLevelBetter];
                    case 7:
                        _e = [_h.sent()];
                        return [4 /*yield*/, this.$swatchSecurityLevelBetter];
                    case 8: return [4 /*yield*/, _d.apply(this, _e.concat([_h.sent()]))];
                    case 9:
                        _h.sent();
                        return [3 /*break*/, 14];
                    case 10:
                        _f = this.clickAndWaitUntilClickable;
                        return [4 /*yield*/, this.$swatchSecurityLevelGood];
                    case 11:
                        _g = [_h.sent()];
                        return [4 /*yield*/, this.$swatchSecurityLevelGood];
                    case 12: return [4 /*yield*/, _f.apply(this, _g.concat([_h.sent()]))];
                    case 13:
                        _h.sent();
                        _h.label = 14;
                    case 14: return [4 /*yield*/, browser.pause(3000)];
                    case 15:
                        _h.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImprintApp.prototype.inputCompanyInformationCheck = function (textInput) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _a = this.clickAndWaitUntilDisplayed;
                        return [4 /*yield*/, this.$companyInformationCheckDropDown];
                    case 1:
                        _b = [_d.sent()];
                        return [4 /*yield*/, this.$addressBlockCheckField];
                    case 2: return [4 /*yield*/, _a.apply(this, _b.concat([_d.sent()]))];
                    case 3:
                        _d.sent();
                        _c = this.addValue;
                        return [4 /*yield*/, this.$addressBlockCheckField];
                    case 4: return [4 /*yield*/, _c.apply(this, [_d.sent(), textInput])];
                    case 5:
                        _d.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImprintApp.prototype.inputCompanyInformationNonCheck = function (textInput) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _a = this.clickAndWaitUntilDisplayed;
                        return [4 /*yield*/, this.$companyInformationNonCheckDropDown];
                    case 1:
                        _b = [_d.sent()];
                        return [4 /*yield*/, this.$addressBlockNonCheckField];
                    case 2: return [4 /*yield*/, _a.apply(this, _b.concat([_d.sent()]))];
                    case 3:
                        _d.sent();
                        _c = this.setValue;
                        return [4 /*yield*/, this.$addressBlockNonCheckField];
                    case 4: return [4 /*yield*/, _c.apply(this, [_d.sent(), textInput])];
                    case 5:
                        _d.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImprintApp.prototype.inputBankInfo = function (bankRtn, bankAcct) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        _a = this.clickAndWaitUntilDisplayed;
                        return [4 /*yield*/, this.$bankInformationDropDown];
                    case 1:
                        _b = [_e.sent()];
                        return [4 /*yield*/, this.$bankRoutingNumberField];
                    case 2: return [4 /*yield*/, _a.apply(this, _b.concat([_e.sent()]))];
                    case 3:
                        _e.sent();
                        _c = this.setValue;
                        return [4 /*yield*/, this.$bankRoutingNumberField];
                    case 4: return [4 /*yield*/, _c.apply(this, [_e.sent(), bankRtn])];
                    case 5:
                        _e.sent();
                        _d = this.setValue;
                        return [4 /*yield*/, this.$bankAccountNumberField];
                    case 6: return [4 /*yield*/, _d.apply(this, [_e.sent(), bankAcct])];
                    case 7:
                        _e.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImprintApp.prototype.approveCheckOrder = function (threeletterinitials) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = this.clickAndWaitUntilDisplayed;
                        return [4 /*yield*/, this.$approvalCheckDropDown];
                    case 1:
                        _b = [_f.sent()];
                        return [4 /*yield*/, this.$approvalButton];
                    case 2: return [4 /*yield*/, _a.apply(this, _b.concat([_f.sent()]))];
                    case 3:
                        _f.sent();
                        _c = this.setValue;
                        return [4 /*yield*/, this.$initialsBox];
                    case 4: return [4 /*yield*/, _c.apply(this, [_f.sent(), threeletterinitials])];
                    case 5:
                        _f.sent();
                        _d = this.clickAndWaitUntilNotDisplayed;
                        return [4 /*yield*/, this.$approvalButton];
                    case 6:
                        _e = [_f.sent()];
                        return [4 /*yield*/, this.$approvalButton];
                    case 7: return [4 /*yield*/, _d.apply(this, _e.concat([_f.sent()]))];
                    case 8:
                        _f.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImprintApp.prototype.approveNonCheckOrder = function (threeletterinitials) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = this.clickAndWaitUntilDisplayed;
                        return [4 /*yield*/, this.$approvalNonCheckDropDown];
                    case 1:
                        _b = [_f.sent()];
                        return [4 /*yield*/, this.$approvalButton];
                    case 2: return [4 /*yield*/, _a.apply(this, _b.concat([_f.sent()]))];
                    case 3:
                        _f.sent();
                        _c = this.setValue;
                        return [4 /*yield*/, this.$initialsBox];
                    case 4: return [4 /*yield*/, _c.apply(this, [_f.sent(), threeletterinitials])];
                    case 5:
                        _f.sent();
                        _d = this.clickAndWaitUntilNotDisplayed;
                        return [4 /*yield*/, this.$approvalButton];
                    case 6:
                        _e = [_f.sent()];
                        return [4 /*yield*/, this.$approvalButton];
                    case 7: return [4 /*yield*/, _d.apply(this, _e.concat([_f.sent()]))];
                    case 8:
                        _f.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImprintApp.prototype.approveOrderUsingDropDownArray = function () {
        return __awaiter(this, void 0, void 0, function () {
            var imprintDropDownsCount, approvalTab, $approvalDropDown, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this.$$arrayOfImprintDropDowns];
                    case 1:
                        imprintDropDownsCount = (_c.sent()).length;
                        approvalTab = imprintDropDownsCount - 1;
                        $approvalDropDown = $("/html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[" + approvalTab + "]/div[1]/h4/a/span/i[1]");
                        console.log(approvalTab);
                        _a = this.clickAndWaitUntilDisplayed;
                        return [4 /*yield*/, $approvalDropDown];
                    case 2:
                        _b = [_c.sent()];
                        return [4 /*yield*/, this.$approvalButton];
                    case 3: return [4 /*yield*/, _a.apply(this, _b.concat([_c.sent()]))];
                    case 4:
                        _c.sent();
                        return [4 /*yield*/, browser.pause(5000)];
                    case 5:
                        _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return ImprintApp;
}(wdioHelper_1.WdioHelper));
exports.ImprintApp = ImprintApp;
//
// Swatch Picker URL https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=59964&d=84613&pt=c
// Swatch Picker URL https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=59967&d=84616&pt=c
//                   https://pftest.greatland.com/greatlanddev/Custom/Steps/SwatchPicker.aspx?p=L1302P4BBG&s=HIGH&u=59968&d=84617&pt=c
//ff_htmlSwatchPicker
//acctNumber: string, rtNumber: string,
//i.icon.ng-scope.icon-pf_btn_next_arrow'
// approval drop down arrow xpaths
// noncheck
// /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[3]/div[1]/h4/a/span/i[1]
// check
// /html/body/div[3]/div[1]/form/div/main/div[2]/aside/div/div/div/div/div[2]/div/div/div/div[1]/div/div/uib-accordion/div/div[6]/div[1]/h4/a/span/i[1]
